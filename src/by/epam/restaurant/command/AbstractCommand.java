package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.ConnectionPool;

/**
 * Parent-class for all of the commands
 */
public abstract class AbstractCommand {
    public static ConnectionPool pool;
    static {
        pool = ConnectionPool.getInstance();
    }

    /**
     * Execution of command
     * @param req object with request parameters
     * @return page to go
     */
    public abstract String execute(SessionRequestContent req);
}
