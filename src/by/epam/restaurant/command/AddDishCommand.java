package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Menu;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.MenuService;

import java.util.ArrayList;

/**
 * Add dish to menu
 */
public class AddDishCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;

        MenuService menuService = new MenuService(wc);
        Menu dish = new Menu();
        dish.setDish(req.getReqParameter("dish"));
        dish.setDescription(req.getReqParameter("description"));
        dish.setPrice(Double.parseDouble(req.getReqParameter("price")));
        menuService.createNew(dish.getDish(), dish.getDescription(), dish.getPrice());
        ArrayList<Menu> menu = menuService.getAll();
        req.setReqAttributes("menu", menu);
        page = ConfigManager.getProperty("path.page.menu");

        pool.closeConnection(wc);
        return page;
    }
}
