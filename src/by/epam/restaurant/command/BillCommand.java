package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderService;

/**
 * Update order to payed
 */
public class BillCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.main");
        int orderId = Integer.parseInt(req.getReqParameter("itemId"));

        OrderService orderSQL = new OrderService(wc);
        Order order = orderSQL.getOrder(orderId);
        orderSQL.update(orderId, order.getClient(), 3, order.getWaiter(), order.getDate());

        pool.closeConnection(wc);
        return page;
    }
}
