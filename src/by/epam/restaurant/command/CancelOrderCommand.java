package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Client;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.ClientService;
import by.epam.restaurant.service.OrderPointService;
import by.epam.restaurant.service.OrderService;

/**
 * Cancel an order by client if it not being send
 */
public class CancelOrderCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        int orderId = Integer.parseInt(req.getReqParameter("orderId"));
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());

        ClientService clientService = new ClientService(wc);
        OrderService orderService = new OrderService(wc);
        OrderPointService orderPointService = new OrderPointService(wc);

        orderPointService.deleteAll(orderId);
        Client client = clientService.findById(id);
        clientService.update(id, client.getName(), client.getLogin(), 0);
        orderService.delete(orderId);
        client = clientService.findById(id);
        req.setReqAttributes("user", client);

        page = ConfigManager.getProperty("path.page.profile");
        pool.closeConnection(wc);
        return page;
    }
}
