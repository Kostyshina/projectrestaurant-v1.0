package by.epam.restaurant.command;

/**
 * Creating the necessary commands
 */
public class CommandFactory {
    /**
     * Creating the necessary command
     * @param command the name of the command
     * @return object required command
     * @throws CommandException if command not listed
     */
    public AbstractCommand defineCommand(String command) throws CommandException{
        try {
            ECommand type = ECommand.valueOf(command.toUpperCase());
            switch (type) {
                case LOCALE: return new LocaleCommand();
                case LOGIN: return new LoginCommand();
                case LOGOUT: return new LogoutCommand();
                case REGISTER: return new RegisterCommand();
                case PROFILE: return new ProfileCommand();
                case EDIT_PROFILE: return new EditProfileCommand();
                case GET_MENU: return new GetMenuCommand();
                case GET_DISH: return new GetDishCommand();
                case ORDER_DISH: return new OrderDishCommand();
                case GET_ORDER: return new GetOrderCommand();
                case EDIT_ORDER: return new EditOrderCommand();
                case CANCEL_ORDER: return new CancelOrderCommand();
                case MAKE_AN_ORDER: return new MakeAnOrderCommand();
                case GET_CLIENT: return new GetClientCommand();
                case TAKE_AN_ORDER: return new TakeAnOrderCommand();
                case BILL: return new BillCommand();
                case ALL_ORDERS: return new GetOrdersCommand();
                case EDIT_DISH: return new EditDishCommand();
                case DELETE_DISH: return new DeleteDishCommand();
                case ADD_DISH: return new AddDishCommand();
                case ALL_WAITERS: return new GetWaitersCommand();
                //case HIRE_WAITER: return new HireWaiterCommand();
                case FIRE_WAITER: return new FireWaiterCommand();
            }
        }catch(IllegalArgumentException e){
            throw new CommandException(command.toUpperCase() + " not present in Enum");
        }
        return null;
    }
}
