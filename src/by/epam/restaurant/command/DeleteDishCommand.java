package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Menu;
import by.epam.restaurant.persistence.model.OrderPoint;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.MenuService;
import by.epam.restaurant.service.OrderPointService;

import java.util.ArrayList;

/**
 * Delete dish from menu
 */
public class DeleteDishCommand extends AbstractCommand{
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        int i = 0;
        int flag = 0;
        int dishId = Integer.parseInt(req.getReqParameter("itemId"));

        MenuService menuService = new MenuService(wc);
        OrderPointService orderService = new OrderPointService(wc);
        ArrayList<OrderPoint> points = orderService.getAll();
        while(i < points.size()){
            int id = points.get(i).getProductId();
            if(id == dishId){
                flag = 1;
                break;
            }
            i++;
        }
        if(flag == 1){
            menuService.deleteDish(dishId);
        } else{
            menuService.delete(dishId);
        }
        ArrayList<Menu> menu = menuService.getAll();
        req.setReqAttributes("menu", menu);

        page = ConfigManager.getProperty("path.page.menu");
        pool.closeConnection(wc);
        return page;
    }
}
