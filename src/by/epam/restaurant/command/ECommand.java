package by.epam.restaurant.command;

/**
 * List of commands
 */
public enum ECommand {
    LOCALE,
    LOGIN,
    LOGOUT,
    REGISTER,
    GET_MENU,
    GET_DISH,
    PROFILE,
    EDIT_PROFILE,

    //client
    ORDER_DISH,
    GET_ORDER,
    EDIT_ORDER,
    CANCEL_ORDER,
    MAKE_AN_ORDER,
    ALL_ORDERS,
    GET_BILL,
    SEND_ORDER,

    //waiter
    GET_CLIENT,
    TAKE_AN_ORDER,
    BILL,

    //admin
    EDIT_DISH,
    DELETE_DISH,
    ADD_DISH,
    ALL_WAITERS,
    FIRE_WAITER,
    HIRE_WAITER
}
