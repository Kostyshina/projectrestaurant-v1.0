package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.persistence.model.OrderPoint;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderPointService;
import by.epam.restaurant.service.OrderService;

/**
 * Delete order points
 */
public class EditOrderCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        int dishId = Integer.parseInt(req.getReqParameter("itemId"));
        int orderId = Integer.parseInt(req.getReqParameter("order"));
        double price = 0;

        OrderService orderService = new OrderService(wc);
        OrderPointService orderPointService = new OrderPointService(wc);
        orderPointService.delete(dishId);
        Order order = orderService.getOrder(orderId);
        for(OrderPoint point : order.getPoints().values()){
            price += point.getDish().getPrice();
        }
        req.setReqAttributes("order", order);
        req.setReqAttributes("cost", price); //альтаирские доллары

        page = ConfigManager.getProperty("path.page.order");
        pool.closeConnection(wc);
        return page;
    }
}
