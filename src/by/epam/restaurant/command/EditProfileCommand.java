package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Client;
import by.epam.restaurant.persistence.model.Waiter;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.ClientService;
import by.epam.restaurant.service.WaiterService;

/**
 * Edit users' data
 */
public class EditProfileCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        int id = Integer.parseInt(req.getReqParameter("id"));
        String login = req.getReqParameter("userLogin");
        String name = req.getReqParameter("userName");
        int orderId = 0;
        String status = req.getSessionAttribute("status").toString();

        if("waiter".equals(status)){
            WaiterService waiterService = new WaiterService(wc);
            waiterService.update(id, name, login);
            Waiter waiter = waiterService.findById(id);
            req.setReqAttributes("user", waiter);
        } else {
            if("client".equals(status)) {
                orderId = Integer.parseInt(req.getReqParameter("userOrder"));
            }
            ClientService clientService = new ClientService(wc);
            clientService.update(id, name, login, orderId);
            Client client = clientService.findById(id);
            req.setReqAttributes("user", client);
        }

        page = ConfigManager.getProperty("path.page.profile");
        pool.closeConnection(wc);
        return page;
    }
}
