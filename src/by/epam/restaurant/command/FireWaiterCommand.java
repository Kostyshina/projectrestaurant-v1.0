package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.persistence.model.Waiter;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderService;
import by.epam.restaurant.service.RegisterService;
import by.epam.restaurant.service.WaiterService;

import java.util.ArrayList;

/**
 * Delete employee from database, if he don't have an order
 */
public class FireWaiterCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.waiters");
        int waiterId = Integer.parseInt(req.getReqParameter("itemId"));
        int i = 0;
        int flag = 0;

        RegisterService registerService = new RegisterService(wc);
        WaiterService waiterService = new WaiterService(wc);
        OrderService orderService = new OrderService(wc);
        ArrayList<Order> orders = orderService.getAll();
        while(i < orders.size()){
            int id = orders.get(i).getWaiter();
            if(id == waiterId){
                flag = 1;
                break;
            }
            i++;
        }
        if(flag == 1) {
            Waiter waiter = waiterService.findById(waiterId);
            waiterService.deleteWaiter(waiterId);
            registerService.deleteWaiter(waiter.getLogin());
        } else{
            Waiter waiter = waiterService.findById(waiterId);
            waiterService.delete(waiterId);
            registerService.delete(waiter.getLogin());
        }
        req.setReqAttributes("waiters", waiterService.getAll());

        pool.closeConnection(wc);
        return page;
    }
}
