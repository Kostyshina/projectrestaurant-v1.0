package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderService;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Select a customer for acceptance of the order
 */
public class GetClientCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        Calendar calendar = Calendar.getInstance();
        WrapperConnector wc = pool.getConnection();
        String page;
        int orderId = Integer.parseInt(req.getReqParameter("itemId"));
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());

        OrderService orderService = new OrderService(wc);
        Order order = orderService.findById(orderId);
        orderService.update(orderId, order.getClient(), order.getCondition(), id, calendar.getTime());

        ArrayList<Order> orders = orderService.getAllWaiter(id);
        ArrayList<Order> newOrders = orderService.getAllNew();
        req.setReqAttributes("orders", orders);
        req.setReqAttributes("newOrders", newOrders);

        page = ConfigManager.getProperty("path.page.orders");
        pool.closeConnection(wc);
        return page;
    }
}
