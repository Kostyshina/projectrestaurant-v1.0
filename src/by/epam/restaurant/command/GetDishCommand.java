package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Menu;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.MenuService;

/**
 * Created by user on 10.05.2015.
 */
public class GetDishCommand extends AbstractCommand{
    public GetDishCommand() {
    }

    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.dish");
        int dishId = Integer.parseInt(req.getReqParameter("itemId"));
        String status = req.getSessionAttribute("status").toString();

        MenuService menuSQL = new MenuService(wc);
        Menu dish = menuSQL.findById(dishId);
        req.setReqAttributes("dish", dish);
        if("admin".equals(status)){
            page = ConfigManager.getProperty("path.page.edit_dish");
        }
        pool.closeConnection(wc);
        return page;
    }
}
