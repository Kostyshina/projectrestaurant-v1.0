package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Menu;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.MenuService;

import java.util.ArrayList;

/**
 * Get menu list
 */
public class GetMenuCommand extends AbstractCommand {
    public GetMenuCommand() {
    }

    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;

        MenuService menuService = new MenuService(wc);
        ArrayList<Menu> menu = menuService.getAll();
        req.setReqAttributes("menu", menu);

        page = ConfigManager.getProperty("path.page.menu");
        pool.closeConnection(wc);
        return page;
    }
}
