package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderService;

import java.util.ArrayList;

/**
 * Get all clients' orders
 */
public class GetOrdersCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());
        String status = req.getSessionAttribute("status").toString();

        OrderService orderService = new OrderService(wc);
        ArrayList<Order> orders = null;
        ArrayList<Order> newOrders = new ArrayList<>();
        switch(status){
            case "client" : {
                orders = orderService.getAll(id);
                break;
            }
            case "waiter" : {
                orders = orderService.getAllWaiter(id);
                newOrders = orderService.getAllNew();
                break;
            }
            case "admin" : {
                orders = orderService.getAll();
                break;
            }
        }
        req.setReqAttributes("orders", orders);
        req.setReqAttributes("newOrders", newOrders);

        page = ConfigManager.getProperty("path.page.orders");
        pool.closeConnection(wc);
        return page;
    }
}
