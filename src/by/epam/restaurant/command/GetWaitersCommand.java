package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.WaiterService;

/**
 * Get list of staff
 */
public class GetWaitersCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.waiters");

        WaiterService waiterService = new WaiterService(wc);
        req.setReqAttributes("waiters", waiterService.getAll());

        pool.closeConnection(wc);
        return page;
    }
}
