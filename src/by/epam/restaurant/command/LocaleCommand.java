package by.epam.restaurant.command;

import by.epam.restaurant.propertiesconf.ConfigManager;

/**
 * Change language
 */
public class LocaleCommand extends AbstractCommand{
    @Override
    public String execute(SessionRequestContent req) {
        String lang = req.getReqParameter("lang");
        String page = ConfigManager.getProperty("path.page.index");
        req.setReqAttributes("lang", lang);
        return page;
    }
}
