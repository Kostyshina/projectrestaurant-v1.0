package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Client;
import by.epam.restaurant.persistence.model.User;
import by.epam.restaurant.persistence.model.Waiter;
import by.epam.restaurant.persistence.security.MD5;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.ClientService;
import by.epam.restaurant.service.RegisterService;
import by.epam.restaurant.service.WaiterService;

import java.util.ArrayList;

/**
 * Log in to system
 */
public class LoginCommand extends AbstractCommand {
    public LoginCommand() {
    }

    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        String username = req.getReqParameter("log");
        String password = MD5.md5Custom(req.getReqParameter("pass"));
        String userAdmin = null;
        int id = 0;
        int flag = 0;
        int i = 0;

        RegisterService registerService = new RegisterService(wc);
        ArrayList<User> users = registerService.getAll();
        while (i < users.size()) {
            String cliLogin = users.get(i).getLogin();
            String cliPassword = users.get(i).getPassword();
            if (cliLogin.equals(username) && cliPassword.equals(password)) {
                flag = 1;
                userAdmin = users.get(i).getStatus();
                break;
            }
            i++;
        }
        i=0;
        if (flag == 1) {
            switch (userAdmin) {
                case "waiter": {
                    WaiterService waiterService = new WaiterService(wc);
                    ArrayList<Waiter> waiters = waiterService.getAll();
                    while (i < waiters.size()){
                        String waiterLogin = waiters.get(i).getLogin();
                        if(waiterLogin.equals(username)){
                            id = waiters.get(i).getId();
                            break;
                        }
                        i++;
                    }
                    break;
                }
                default: {
                    ClientService clientService = new ClientService(wc);
                    ArrayList<Client> clients = clientService.getAll();
                    while (i < clients.size()) {
                        String cliLogin = clients.get(i).getLogin();
                        if (cliLogin.equals(username)) {
                            id = clients.get(i).getId();
                            break;
                        }
                        i++;
                    }
                }
            }
            req.setSessionAttributes("ID", id);
            req.setSessionAttributes("identifier", username);
            req.setSessionAttributes("status", userAdmin);
            page = ConfigManager.getProperty("path.page.main");
        }
        else {
            page = ConfigManager.getProperty("path.page.index");
        }

        pool.closeConnection(wc);
        return page;
    }
}
