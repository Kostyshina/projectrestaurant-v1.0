package by.epam.restaurant.command;

import by.epam.restaurant.propertiesconf.ConfigManager;

/**
 * Sing out from the system
 */
public class LogoutCommand extends AbstractCommand{
    public LogoutCommand() {
    }

    @Override
    public String execute(SessionRequestContent req) {
        String page;
        //проверка оплаты чека
        page = ConfigManager.getProperty("path.page.index");
        return page;
    }
}
