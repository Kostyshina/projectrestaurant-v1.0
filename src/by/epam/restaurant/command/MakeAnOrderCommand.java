package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.propertiesconf.MessageManager;
import by.epam.restaurant.service.OrderService;

import java.util.Calendar;

/**
 * Customer makes an order
 */
public class MakeAnOrderCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        Calendar calendar = Calendar.getInstance();
        MessageManager messageManager = new MessageManager();
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.main");
        int orderId = Integer.parseInt(req.getReqParameter("id"));
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());

        OrderService orderService = new OrderService(wc);
        Order order = orderService.getOrder(orderId);
        if(order.getWaiter() == 0){
            page = ConfigManager.getProperty("path.page.message");
            req.setReqAttributes("message", messageManager.getProperty("message.order.waiter"));
        } else {
            orderService.update(orderId, id, 1, order.getWaiter(), calendar.getTime());
        }

        pool.closeConnection(wc);
        return page;
    }
}
