package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Client;
import by.epam.restaurant.persistence.model.Menu;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.ClientService;
import by.epam.restaurant.service.MenuService;
import by.epam.restaurant.service.OrderPointService;
import by.epam.restaurant.service.OrderService;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Add the item to the order
 */
public class OrderDishCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        Calendar calendar = Calendar.getInstance();
        WrapperConnector wc = pool.getConnection();
        String page;
        int orderId;
        int dishId = Integer.parseInt(req.getReqParameter("id"));
        int quantity = Integer.parseInt(req.getReqParameter("quantity"));
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());

        MenuService menuService = new MenuService(wc);
        ClientService clientService = new ClientService(wc);
        OrderService orderService = new OrderService(wc);
        OrderPointService orderPointService = new OrderPointService(wc);
        Client client = clientService.findById(id);
        if(client.getOrder() != 0){
            Order order = orderService.findById(client.getOrder());
            if(order.getCondition() == 0){
                orderService.update(order.getId(), order.getClient(), order.getCondition(), 0, calendar.getTime());
                orderId = order.getId();
            }
            else {
                orderId = orderService.createNew(client.getId(), 0, calendar.getTime());
            }
        }
        else{
            orderId = orderService.createNew(client.getId(), 0, calendar.getTime());
        }
        orderPointService.createNew(orderId, dishId, quantity);
        clientService.update(id, client.getName(), client.getLogin(), orderId);
        ArrayList<Menu> menu = menuService.getAll();
        req.setReqAttributes("menu", menu);

        page = ConfigManager.getProperty("path.page.menu");
        pool.closeConnection(wc);
        return page;
    }
}
