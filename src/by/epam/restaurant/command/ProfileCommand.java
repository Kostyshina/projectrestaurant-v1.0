package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Client;
import by.epam.restaurant.persistence.model.Waiter;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.ClientService;
import by.epam.restaurant.service.WaiterService;

/**
 * Go to profile command
 */
public class ProfileCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page;
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());
        String status = req.getSessionAttribute("status").toString();

        if("waiter".equals(status)){
            WaiterService waiterService = new WaiterService(wc);
            Waiter waiter = waiterService.findById(id);
            req.setReqAttributes("user", waiter);
        } else {
            ClientService clientService = new ClientService(wc);
            Client client = clientService.findById(id);
            req.setReqAttributes("user", client);

        }

        page = ConfigManager.getProperty("path.page.profile");
        pool.closeConnection(wc);
        return page;
    }
}
