package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.User;
import by.epam.restaurant.persistence.security.MD5;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.propertiesconf.MessageManager;
import by.epam.restaurant.service.ClientService;
import by.epam.restaurant.service.RegisterService;
import by.epam.restaurant.service.WaiterService;

import java.util.ArrayList;

/**
 * Registration
 */
public class RegisterCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        MessageManager messageManager = new MessageManager();
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.index");

        RegisterService registerService = new RegisterService(wc);
        ClientService clientService = new ClientService(wc);
        WaiterService waiterService = new WaiterService(wc);
        String password = MD5.md5Custom(req.getReqParameter("password"));
        String rpassword = MD5.md5Custom(req.getReqParameter("rpassword"));

        if (!rpassword.equals(password)) {
            req.setReqAttributes("error", messageManager.getProperty("message.register.wrongpassword")); //2 message файла
        } else {
            String login = req.getReqParameter("login");
            int flag = 0;
            int i = 0;
            ArrayList<User> users = registerService.getAll();
            while (i < users.size()) {
                String cliLogin = users.get(i).getLogin();
                if (cliLogin.equals(login)) {
                    flag = 1;
                    break;
                }
                i++;
            }
            if (flag == 1) {
                req.setReqAttributes("error", messageManager.getProperty("message.register.wronglogin"));
            } else {
                if(req.getSessionAttribute("status") == null){
                    registerService.createNew(login, password, "client");
                    clientService.createNew("", login, 0);
                } else {
                    String status = req.getSessionAttribute("status").toString();
                    if("admin".equals(status)){
                        registerService.createNew(login, password, "waiter");
                        waiterService.createNew("", login);
                    }
                }
            }
        }
        pool.closeConnection(wc);
        return page;
    }
}
