package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderService;

/**
 * Created by user on 11.05.2015.
 */
public class SendOrderCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.main");
        int orderId = Integer.parseInt(req.getReqParameter("orderId"));
        OrderService orderSQL = new OrderService(wc);
        Order order = orderSQL.findById(orderId);
        orderSQL.update(orderId, order.getClient(), 1, order.getWaiter(), order.getDate());
        pool.closeConnection(wc);
        return page;
    }
}
