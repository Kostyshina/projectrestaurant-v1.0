package by.epam.restaurant.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;

/**
 * Class to work with request at the lower levels
 */
public class SessionRequestContent {
    private HashMap<String, Object> reqAttributes;
    private HashMap<String, String[]> reqParameters;
    private HashMap<String, Object> sessionAttributes;

    public SessionRequestContent() {
        reqAttributes = new HashMap<>();
        reqParameters = new HashMap<>();
        sessionAttributes = new HashMap<>();
    }

    /**
     * Taking elements from request
     * @param req transmittable request
     */
    public void extractValues(HttpServletRequest req) {
        reqParameters = new HashMap<>(req.getParameterMap());
        HttpSession session = req.getSession(true);
        String key;
        Enumeration<String> names = session.getAttributeNames();
        while(names.hasMoreElements()){
            key = names.nextElement();
            sessionAttributes.put(key, session.getAttribute(key));
        }
    }

    /**
     * Adding parameters to request
     * @param req transmittable request
     */
    public void insertAttributes(HttpServletRequest req) {
        HttpSession session = req.getSession(true);
        for(String key : sessionAttributes.keySet()){
            session.setAttribute(key, sessionAttributes.get(key));
        }
        for(String key : reqAttributes.keySet()){
            req.setAttribute(key, reqAttributes.get(key));
        }
    }

    public String getReqParameter(String name){
        return reqParameters.get(name)[0];
    }

    public Object getSessionAttribute(String name){
        return sessionAttributes.get(name);
    }

    public void setReqAttributes(String name, Object value){
        reqAttributes.put(name, value);
    }

    public void setSessionAttributes(String name, Object value){
        sessionAttributes.put(name, value);
    }

    public void deleteSession(HttpServletRequest req){
        HttpSession session = req.getSession();
        sessionAttributes.clear();
        session.invalidate();
    }
}
