package by.epam.restaurant.command;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.propertiesconf.ConfigManager;
import by.epam.restaurant.service.OrderService;

/**
 * Acceptance of the order for execution
 */
public class TakeAnOrderCommand extends AbstractCommand {
    @Override
    public String execute(SessionRequestContent req) {
        WrapperConnector wc = pool.getConnection();
        String page = ConfigManager.getProperty("path.page.main");
        int orderId = Integer.parseInt(req.getReqParameter("id"));
        int id = Integer.parseInt(req.getSessionAttribute("ID").toString());

        OrderService orderService = new OrderService(wc);
        Order order = orderService.getOrder(orderId);
        orderService.update(orderId, order.getClient(), 2, id, order.getDate());

        pool.closeConnection(wc);
        return page;
    }
}
