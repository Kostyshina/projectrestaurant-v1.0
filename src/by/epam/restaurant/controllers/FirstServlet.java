package by.epam.restaurant.controllers;

import by.epam.restaurant.command.AbstractCommand;
import by.epam.restaurant.command.CommandException;
import by.epam.restaurant.command.CommandFactory;
import by.epam.restaurant.command.SessionRequestContent;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Commands' servlet
 */
@WebServlet("/controller")
public class FirstServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(FirstServlet.class);

    @Override
    public void init() throws ServletException {
        String logFileName = "WEB-INF/properties/log4j.properties";
        String pref = getServletContext().getRealPath("/");
        PropertyConfigurator.configure(pref + logFileName);
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        doCommand(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        doCommand(req, resp);
    }

    private void doCommand(HttpServletRequest req, HttpServletResponse resp){
        String page;
        try {
            CommandFactory user = new CommandFactory();
            AbstractCommand command = user.defineCommand(req.getParameter("command"));
            SessionRequestContent content = new SessionRequestContent();
            content.extractValues(req);
            page = command.execute(content);
            if("logout".equalsIgnoreCase(req.getParameter("command"))){
                content.deleteSession(req);
            }
            content.insertAttributes(req);
            if(page != null){
                RequestDispatcher dispatcher = req.getRequestDispatcher(page);
                dispatcher.forward(req, resp);
            }else{
                RequestDispatcher dispatcher = req.getRequestDispatcher("/jsp/error/error_page.jsp");
                dispatcher.forward(req, resp);
            }
        } catch(ServletException | IOException | CommandException e){
            LOG.error(e.getMessage());
            //error page send redirect
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        AbstractCommand.pool.destroyPool();
    }
}
