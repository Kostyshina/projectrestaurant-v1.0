package by.epam.restaurant.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Suppresses direct access to a page
 */

@WebFilter(
        urlPatterns = { "/jsp/*" },
        initParams = { @WebInitParam(name = "indexPath", value = "/index.jsp"),
                @WebInitParam(name = "mainPath", value = "/jsp/main.jsp") })
public class FilterJSP implements Filter{
    private String indexPath;
    private String mainPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        indexPath = filterConfig.getInitParameter("indexPath");
        mainPath = filterConfig.getInitParameter("mainPath");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        if(session.getAttribute("identifier") == null) {
            httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
        }
        if(httpRequest.getRequestURL().indexOf("controller") == -1
                && (!"/jsp/main.jsp".equals(httpRequest.getRequestURI())
                && !"index.jsp".equals(httpRequest.getRequestURI()))){
            httpResponse.sendRedirect(httpRequest.getContextPath() + mainPath);
        }
        else{
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
