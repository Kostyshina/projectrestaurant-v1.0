package by.epam.restaurant.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Suppresses access to a commands
 */
@WebFilter(
        urlPatterns = {"/controller"},
        initParams = {@WebInitParam(name = "indexPath", value = "/index.jsp"),
                @WebInitParam(name = "mainPath", value = "/jsp/main.jsp") })
public class FilterServlet implements Filter {
    private String indexPath;
    private String mainPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        indexPath = filterConfig.getInitParameter("indexPath");
        mainPath = filterConfig.getInitParameter("mainPath");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String command = request.getParameter("command");
        HttpSession session = httpRequest.getSession();

        if ((session.getAttribute("identifier")) == null) {
            if (!"Locale".equalsIgnoreCase(command) && !"login".equalsIgnoreCase(command) && !"register".equalsIgnoreCase(command)) {
                httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
            }
            else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
