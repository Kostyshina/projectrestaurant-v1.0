package by.epam.restaurant.persistence.connector;

import org.apache.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Thread-safe singleton. Field - queue of connections
 * Initialization on Demand Holder
 */
public class ConnectionPool {
    private BlockingQueue<WrapperConnector> connectors;
    private static final Logger LOG = Logger.getLogger(ConnectionPool.class);
    private static class PoolHolder{
        private static final ConnectionPool INSTANCE = new ConnectionPool(32);//размер из properties
    }

    private ConnectionPool(int poolSize){
        connectors = new ArrayBlockingQueue<>(poolSize);
        for(int i = 0; i < poolSize; i++){
            WrapperConnector wc = new WrapperConnector();
            connectors.offer(wc);
        }
    }

    public static ConnectionPool getInstance(){
        return PoolHolder.INSTANCE;
    }

    public WrapperConnector getConnection() {
        WrapperConnector wc = null;
        try {
            wc = connectors.take();
        } catch(InterruptedException e){
            LOG.error(e.getMessage());
        }
        return wc;
    }

    /**
     * Return to the pool
     * @param wc - connection, which is returned
     */
    public void closeConnection(WrapperConnector wc) {
        connectors.offer(wc);
    }

    /**
     * Close all connections
     */
    public void destroyPool() {
        try {
            for (int i = 0; i < connectors.size(); i++) {
                WrapperConnector wc = connectors.take();
                wc.closeConnection();
            }
        } catch (InterruptedException e){
            LOG.error(e.getMessage());
        }
    }
}
