package by.epam.restaurant.persistence.connector;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class-connector to database
 */
public class ConnectorDB {
    private static final Logger LOG = Logger.getLogger(ConnectorDB.class);
    private static final String dbProperties ="resources/database.properties";

    /**
     * Driver registration
     */
    static {
        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
        } catch (SQLException e){
            LOG.fatal(e);
            throw new ExceptionInInitializerError("Driver not found" + e.getMessage());
        }
    }

    /**
     * Connection to DB
     * @return connection
     * @see java.sql.Connection
     */
    public static Connection getConnection() {
        ClassLoader loader = ConnectorDB.class.getClassLoader();
        Connection connection;
        Properties properties = new Properties();
        URL url = loader.getResource(dbProperties);
        try {
            if(url != null) {
                properties.load(url.openStream());
            }
            connection = DriverManager.getConnection(properties.getProperty("url"),properties);
        }catch (IOException | SQLException e){
            LOG.fatal(e);
            throw new ExceptionInInitializerError("Unable to connect to database" + e.getMessage());
        }
        return connection;
    }
}
