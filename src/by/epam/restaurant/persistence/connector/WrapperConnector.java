package by.epam.restaurant.persistence.connector;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Wrapper for connection
 */
public class WrapperConnector {
    private static final Logger LOG = Logger.getLogger(WrapperConnector.class);
    private Connection connect;
    public WrapperConnector() {
        connect = ConnectorDB.getConnection();
    }

    public PreparedStatement getStatement(String sql) {     //??????????????????
        try {
            if (connect != null) {
                PreparedStatement prsmt = connect.prepareStatement(sql);
                if (prsmt != null) {
                    return prsmt;
                }
            }
        } catch(SQLException e){
            LOG.error("Connection or statement is null");
        }
        return null;
    }

    public void closeConnection() {
        if (connect != null) {
            try {
                connect.close();
            } catch (SQLException e) {
                LOG.error("Wrong connection "+e.getMessage());
            }
        }
    }
}
