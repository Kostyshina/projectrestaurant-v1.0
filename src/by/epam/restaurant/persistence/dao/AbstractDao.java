package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Entity;

import java.util.ArrayList;

/**
 * Class working with database
 */
public abstract class AbstractDao <K, T extends Entity> {
    protected WrapperConnector connector;           //pool ?

    public abstract ArrayList<T> getAll();
    public abstract T findById(K id);
    public abstract boolean delete(K id);
    //public abstract boolean delete(T entity);
    public abstract K create(T entity);
    public abstract T update(T entity);

}
