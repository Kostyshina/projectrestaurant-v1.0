package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Client;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 * {@inheritDoc}
 * client db
 */
public class ClientDao extends AbstractDao<Integer, Client> {
    private static final Logger LOG = Logger.getLogger(ClientDao.class);
    private final String SELECT_BY_ID = "SELECT * FROM client WHERE client_id = ?";
    private final String INSERT_CLIENT = "INSERT INTO client (client_id, name, login_name) " +
            "VALUES (client_seq.nextval, ?, ?)";
    private final String UPDATE_CLIENT = "UPDATE client SET name = ?, login_name = ?, order_number = ?"+
            "WHERE client_id = ?";
    private final String DELETE_CLIENT = "DELETE FROM client WHERE client_id = ?";
    private final String SELECT_ALL = "SELECT * FROM client";

    public ClientDao(WrapperConnector wc) {
        connector = wc;
    }

    @Override
    public ArrayList<Client> getAll() {
        ArrayList<Client> clients = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                Client client = new Client();
                client.setId(resultSet.getInt("client_id"));
                client.setName(resultSet.getString("name"));
                client.setLogin(resultSet.getString("login_name"));
                client.setOrder(resultSet.getInt("order_number"));
                clients.add(i++, client);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return clients;
    }

    @Override
    public Client findById(Integer clientId) {
        Client client = new Client();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_ID)) {
            pstmt.setInt(1, clientId);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                client.setId(clientId);
                client.setName(resultSet.getString("name"));
                client.setLogin(resultSet.getString("login_name"));
                client.setOrder(resultSet.getInt("order_number"));
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return client;
    }

    @Override
    public boolean delete(Integer clientId) {
        try (PreparedStatement pstmt = connector.getStatement(DELETE_CLIENT)) {
            pstmt.setLong(1, clientId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }

    @Override
    public Integer create(Client client) {
        try (PreparedStatement pstmt = connector.getStatement(INSERT_CLIENT)) {
            pstmt.setString(1, client.getName());
            pstmt.setString(2, client.getLogin());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    @Override
    public Client update(Client client) {
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_CLIENT)) {
            Client old = findById(client.getId());
            pstmt.setString(1, client.getName());
            if(client.getLogin() != null)
                pstmt.setString(2, client.getLogin());
            else{
                client.setLogin(old.getLogin());
                pstmt.setString(2, old.getLogin());
            }
            if(client.getOrder() != 0)
                pstmt.setInt(3, client.getOrder());
            else{
                pstmt.setNull(3, Types.INTEGER);
            }
            pstmt.setInt(4, client.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            LOG.error(e.getMessage());
        }
        return client;
    }
}
