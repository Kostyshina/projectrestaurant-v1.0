package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Menu;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 * {@inheritDoc}
 * client db
 */
public class MenuDao extends AbstractDao<Integer, Menu>{
    private static final Logger LOG = Logger.getLogger(MenuDao.class);
    private final String SELECT_BY_ID = "SELECT * FROM menu WHERE dish_id = ?";
    private final String INSERT_DISH = "INSERT INTO menu (dish_id, dish, description, price) " +
            "VALUES (menu_seq.nextval, ?, ?, ?)";
    private final String UPDATE_STATUS = "UPDATE menu SET status = ? WHERE dish_id = ?";
    private final String UPDATE_DISH = "UPDATE menu SET dish = ?, description = ?, price = ? WHERE dish_id = ?";
    private final String DELETE_DISH = "DELETE FROM menu WHERE dish_id = ?";
    private final String SELECT_ALL = "SELECT * FROM menu";

    public MenuDao(WrapperConnector wc){ connector = wc; }

    @Override
    public ArrayList<Menu> getAll() {
        ArrayList<Menu> dishes = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                if(resultSet.getInt("status") == 0) {
                    Menu dish = new Menu();
                    dish.setId(resultSet.getInt("dish_id"));
                    dish.setDish(resultSet.getString("dish"));
                    dish.setDescription(resultSet.getString("description"));
                    dish.setPrice(resultSet.getDouble("price"));
                    dishes.add(i++, dish);
                }
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return dishes;
    }

    @Override
    public Menu findById(Integer dishId) {
        Menu dish = new Menu();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_ID)) {
            pstmt.setInt(1, dishId);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                dish.setId(dishId);
                dish.setDish(resultSet.getString("dish"));
                dish.setDescription(resultSet.getString("description"));
                dish.setPrice(resultSet.getDouble("price"));
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return dish;
    }

    @Override
    public boolean delete(Integer dishId) {
        try (PreparedStatement pstmt = connector.getStatement(DELETE_DISH)) {
            pstmt.setLong(1, dishId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }


    @Override
    public Integer create(Menu dish) {
        try (PreparedStatement pstmt = connector.getStatement(INSERT_DISH)) {
            pstmt.setString(1, dish.getDish());
            pstmt.setString(2, dish.getDescription());
            pstmt.setDouble(3, dish.getPrice());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    @Override
    public Menu update(Menu dish) {
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_DISH)) {
            pstmt.setString(1, dish.getDish());
            if(dish.getDescription() != null){
                pstmt.setString(2, dish.getDescription());
            } else{
                pstmt.setNull(2, Types.VARCHAR);
            }
            pstmt.setDouble(3, dish.getPrice());
            pstmt.setInt(4, dish.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return dish;
    }

    public boolean updateStatus(Integer dishId){
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_STATUS)) {
            pstmt.setInt(1, 1);         //1 - modified ir deleted menu dish
            pstmt.setInt(2, dishId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }
}
