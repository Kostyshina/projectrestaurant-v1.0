package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Order;
import by.epam.restaurant.persistence.model.Waiter;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;

/**
 * {@inheritDoc}
 * orders db
 */
public class OrderDao extends AbstractDao<Integer, Order> {
    private static final Logger LOG = Logger.getLogger(OrderDao.class);
    private final String SELECT_BY_ID = "SELECT * FROM cli_order WHERE order_id = ?";
    private final String SELECT_BY_DATE = "SELECT * FROM cli_order WHERE date_time = ?";
    private final String INSERT_ORDER = "INSERT INTO cli_order (order_id, client, date_time) " +
            "VALUES (cli_order_seq.nextval, ?, ?)";
    private final String UPDATE_ORDER = "UPDATE cli_order SET condition = ?, waiter = ?, date_time = ? WHERE order_id = ?";
    private final String DELETE_ORDER = "DELETE FROM cli_order WHERE order_id = ?";
    private final String SELECT_ALL = "SELECT cli_order.*, state FROM cli_order " +
            "INNER JOIN order_status ON cli_order.CONDITION = order_status.STATUS_ID";
    private final String SELECT_ALL_NEW = "SELECT cli_order.*, state FROM cli_order " +
            "INNER JOIN order_status ON cli_order.CONDITION = order_status.STATUS_ID " +
            "WHERE state = 'new'";
    private final String SELECT_ALL_CLIENT = "SELECT cli_order.*, state FROM cli_order " +
            "INNER JOIN order_status ON cli_order.CONDITION = order_status.STATUS_ID " +
            "WHERE client = ?";
    private final String SELECT_ALL_WAITER = "SELECT cli_order.*, state FROM cli_order " +
            "INNER JOIN waiter ON cli_order.WAITER = waiter.WAITER_ID " +
            "INNER JOIN order_status ON cli_order.CONDITION = order_status.STATUS_ID " +
            "WHERE waiter_id = ?";
    private final String SELECT_ORDER = "SELECT cli_order.*, state FROM cli_order " +
            "INNER JOIN order_status ON cli_order.CONDITION = order_status.STATUS_ID " +
            "WHERE order_id = ?";

    public OrderDao(WrapperConnector wc) {
        connector = wc;
    }

    public Order getOrder(Integer orderId){
        Order order = new Order();
        OrderPointDao pointDao = new OrderPointDao(connector);
        WaiterDao waiterDao = new WaiterDao(connector);
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ORDER)) {
            pstmt.setInt(1, orderId);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                order.setId(orderId);
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(resultSet.getTimestamp("date_time"));

                if(order.getWaiter() != 0){
                    Waiter waiter = waiterDao.findById(order.getWaiter());
                    order.setWaiterName(waiter.getName());
                }
                order.setConditionState(resultSet.getString("state"));
                order.setPoints(pointDao.getOrder(orderId));
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return order;
    }

    public ArrayList<Order> getAll(Integer client){
        ArrayList<Order> orders = new ArrayList<>();
        OrderPointDao pointDao = new OrderPointDao(connector);
        WaiterDao waiterDao = new WaiterDao(connector);
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL_CLIENT)) {
            int i = 0;
            pstmt.setInt(1, client);
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("order_id"));
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(resultSet.getTimestamp("date_time"));

                if(order.getWaiter() != 0){
                    Waiter waiter = waiterDao.findById(order.getWaiter());
                    order.setWaiterName(waiter.getName());
                }
                order.setConditionState(resultSet.getString("state"));
                order.setPoints(pointDao.getOrder(order.getId()));
                orders.add(i++, order);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    public ArrayList<Order> getAllWaiter(Integer waiter){
        ArrayList<Order> orders = new ArrayList<>();
        OrderPointDao pointDao = new OrderPointDao(connector);
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL_WAITER)) {
            int i = 0;
            pstmt.setInt(1, waiter);
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("order_id"));
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(resultSet.getTimestamp("date_time"));

                order.setConditionState(resultSet.getString("state"));
                order.setPoints(pointDao.getOrder(order.getId()));
                orders.add(i++, order);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    public ArrayList<Order> getAllNew(){
        ArrayList<Order> orders = new ArrayList<>();
        OrderPointDao pointDao = new OrderPointDao(connector);
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL_NEW)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("order_id"));
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(resultSet.getTimestamp("date_time"));

                order.setConditionState(resultSet.getString("state"));
                order.setPoints(pointDao.getOrder(order.getId()));
                orders.add(i++, order);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    @Override
    public ArrayList<Order> getAll() {
        ArrayList<Order> orders = new ArrayList<>();
        OrderPointDao pointDao = new OrderPointDao(connector);
        WaiterDao waiterDao = new WaiterDao(connector);
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt("order_id"));
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(resultSet.getTimestamp("date_time"));

                if(order.getWaiter() != 0){
                    Waiter waiter = waiterDao.findById(order.getWaiter());
                    order.setWaiterName(waiter.getName());
                }

                order.setConditionState(resultSet.getString("state"));
                order.setPoints(pointDao.getOrder(order.getId()));
                orders.add(i++, order);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return orders;
    }

    private Order findByDate(Date date){
        Order order = new Order();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_DATE)) {
            pstmt.setTimestamp(1, new java.sql.Timestamp(date.getTime()));
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                order.setId(resultSet.getInt("order_id"));
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));//Брать из таблицы order_status
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(date);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return order;
    }

    @Override
    public Order findById(Integer orderId) {
        Order order = new Order();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_ID)) {
            pstmt.setInt(1, orderId);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                order.setId(orderId);
                order.setClient(resultSet.getInt("client"));
                order.setCondition(resultSet.getInt("condition"));
                order.setWaiter(resultSet.getInt("waiter"));
                order.setDate(resultSet.getTimestamp("date_time"));
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return order;
    }

    @Override
    public boolean delete(Integer orderId) {
        try (PreparedStatement pstmt = connector.getStatement(DELETE_ORDER)) {
            pstmt.setLong(1, orderId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }

    @Override
    public Integer create(Order order) {
        try (PreparedStatement pstmt = connector.getStatement(INSERT_ORDER)) {
            pstmt.setInt(1, order.getClient());
            pstmt.setTimestamp(2, new java.sql.Timestamp(order.getDate().getTime()));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        order = findByDate(order.getDate());
        return order.getId();
    }

    @Override
    public Order update(Order order) {
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_ORDER)) {
            Order old = findById(order.getId());
            pstmt.setInt(1, order.getCondition());
            if(order.getWaiter() != 0)
                pstmt.setInt(2, order.getWaiter());
            else{
                pstmt.setNull(2, Types.INTEGER);
            }
            if(order.getDate() != null)
                pstmt.setTimestamp(3, new java.sql.Timestamp(order.getDate().getTime()));
            else{
                order.setDate(old.getDate());
                pstmt.setTimestamp(3, new java.sql.Timestamp(old.getDate().getTime()));
            }
            pstmt.setInt(4, order.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return order;
    }
}
