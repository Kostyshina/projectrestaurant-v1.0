package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Menu;
import by.epam.restaurant.persistence.model.OrderPoint;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * {@inheritDoc}
 * order points db
 */
public class OrderPointDao extends AbstractDao<Integer, OrderPoint> {
    private static final Logger LOG = Logger.getLogger(OrderPointDao.class);
    private final String SELECT_BY_ID = "SELECT * FROM menu_pos WHERE id = ?";
    private final String INSERT_POINT = "INSERT INTO menu_pos (id, id_order, id_product, quantity) " +
            "VALUES (menu_pos_seq.nextval, ?, ?, ?)";
    private final String UPDATE_POINT = "UPDATE menu_pos SET id_order = ?, id_product = ?, quantity = ? WHERE id = ?";
    private final String DELETE_POINT = "DELETE FROM menu_pos WHERE id = ?";
    private final String DELETE_ORDER = "DELETE FROM menu_pos WHERE id_order = ?";
    private final String SELECT_ALL = "SELECT * FROM menu_pos";
    private final String SELECT_ALL_ORDER = "SELECT * FROM menu_pos WHERE id_order = ?";
    private final String SELECT_ORDER = "SELECT id, dish, description, price, " +
            "quantity, id_product FROM menu_pos " +
            "INNER JOIN menu ON id_product = dish_id " +
            "WHERE id_order = ?";

    public OrderPointDao(WrapperConnector wc) {
        connector = wc;
    }

    public ArrayList<OrderPoint> getOrder(Integer order){
        ArrayList<OrderPoint> points = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ORDER)) {
            int i = 0;
            pstmt.setInt(1, order);
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                OrderPoint point = new OrderPoint();
                Menu dish = new Menu();
                point.setOrderId(order);
                point.setId(resultSet.getInt("id"));
                point.setProductId(resultSet.getInt("id_product"));
                point.setQuantity(resultSet.getInt("quantity"));
                dish.setDish(resultSet.getString("dish"));
                dish.setDescription(resultSet.getString("description"));
                dish.setPrice(resultSet.getDouble("price"));
                point.setDish(dish);
                points.add(i++, point);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return points;
    }

    public ArrayList<OrderPoint> getAll(Integer order){
        ArrayList<OrderPoint> points = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL_ORDER)) {
            int i = 0;
            pstmt.setInt(1, order);
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                OrderPoint point = new OrderPoint();
                point.setOrderId(order);
                point.setId(resultSet.getInt("id"));
                point.setProductId(resultSet.getInt("id_product"));
                point.setQuantity(resultSet.getInt("quantity"));
                points.add(i++, point);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return points;
    }

    @Override
    public ArrayList<OrderPoint> getAll() {
        ArrayList<OrderPoint> points = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                OrderPoint point = new OrderPoint();
                point.setId(resultSet.getInt("id"));
                point.setOrderId(resultSet.getInt("id_order"));
                point.setProductId(resultSet.getInt("id_product"));
                point.setQuantity(resultSet.getInt("quantity"));
                points.add(i++, point);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return points;
    }

    @Override
    public OrderPoint findById(Integer pointId) {
        OrderPoint point = new OrderPoint();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_ID)) {
            pstmt.setInt(1, pointId);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                point.setId(pointId);
                point.setOrderId(resultSet.getInt("id_order"));
                point.setProductId(resultSet.getInt("id_product"));
                point.setQuantity(resultSet.getInt("quantity"));
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return point;
    }

    public boolean deleteAll(Integer orderId){
        try (PreparedStatement pstmt = connector.getStatement(DELETE_ORDER)) {
            pstmt.setInt(1, orderId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }

    @Override
    public boolean delete(Integer pointId) {
        try (PreparedStatement pstmt = connector.getStatement(DELETE_POINT)) {
            pstmt.setInt(1, pointId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }

    @Override
    public Integer create(OrderPoint point) {
        try (PreparedStatement pstmt = connector.getStatement(INSERT_POINT)) {
            pstmt.setInt(1, point.getOrderId());
            pstmt.setInt(2, point.getProductId());
            pstmt.setInt(3, point.getQuantity());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    @Override
    public OrderPoint update(OrderPoint point) {
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_POINT)) {
            pstmt.setInt(1, point.getOrderId());
            pstmt.setInt(2, point.getProductId());
            pstmt.setInt(3, point.getQuantity());
            pstmt.setInt(4, point.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return point;
    }
}
