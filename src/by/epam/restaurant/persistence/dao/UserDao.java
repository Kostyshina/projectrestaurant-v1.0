package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.User;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * {@inheritDoc}
 * db registr
 */
public class UserDao extends AbstractDao<String, User>{
    private static final Logger LOG = Logger.getLogger(UserDao.class);
    private final String SELECT_BY_ID = "SELECT * FROM registr WHERE login = ?";
    private final String INSERT_USER = "INSERT INTO registr (login, password, status) VALUES (?, ?, ?)";
    private final String UPDATE_USER = "UPDATE registr SET password = ?, status = ? WHERE login = ?";
    private final String UPDATE_STATUS = "UPDATE registr SET status = ? WHERE login = ?";
    private final String DELETE_USER = "DELETE FROM registr WHERE login = ?";
    private final String SELECT_ALL = "SELECT * FROM registr";

    public UserDao(WrapperConnector wc) { connector = wc; }

    @Override
    public ArrayList<User> getAll() {
        ArrayList<User> users = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getNString("password"));
                user.setStatus(resultSet.getString("status"));
                users.add(i++, user);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return users;
    }

    @Override
    public User findById(String login) {
        User user = new User();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_ID)) {
            pstmt.setString(1, login);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                user.setLogin(login);
                user.setPassword(resultSet.getNString("password"));
                user.setLogin(resultSet.getString("status"));
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return user;
    }

    @Override
    public boolean delete(String login) {
        try (PreparedStatement pstmt = connector.getStatement(DELETE_USER)) {
            pstmt.setString(1, login);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }

    @Override
    public String create(User user) {
        try (PreparedStatement pstmt = connector.getStatement(INSERT_USER)) {
            pstmt.setString(1, user.getLogin());
            pstmt.setNString(2, user.getPassword());
            pstmt.setString(3, user.getStatus());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    public User updateStatus(String login){
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_STATUS)){
            pstmt.setString(1, "client");
            pstmt.setString(2, login);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e);
        }
        return findById(login);
    }

    @Override
    public User update(User user) {
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_USER)) {
            User old = findById(user.getLogin());
            pstmt.setString(1, user.getLogin());
            if(user.getPassword() != null)
                pstmt.setNString(2, user.getPassword());
            else{
                user.setPassword(old.getPassword());
                pstmt.setNString(2, old.getPassword());
            }
            if(user.getStatus() != null)
                pstmt.setString(3, user.getStatus());
            else{
                user.setStatus(old.getStatus());
                pstmt.setString(3, old.getStatus());
            }
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return user;
    }
}
