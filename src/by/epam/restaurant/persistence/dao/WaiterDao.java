package by.epam.restaurant.persistence.dao;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.model.Waiter;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * {@inheritDoc}
 * waiter db
 */
public class WaiterDao extends AbstractDao<Integer, Waiter> {
    private static final Logger LOG = Logger.getLogger(WaiterDao.class);
    private final String SELECT_BY_ID = "SELECT * FROM waiter WHERE waiter_id = ?";
    private final String INSERT_WAITER = "INSERT INTO waiter (waiter_id, full_name, register_login) " +
            "VALUES (waiter_seq.nextval, ?, ?)";
    private final String UPDATE_WAITER = "UPDATE waiter SET full_name = ?, register_login = ?"+
            "WHERE waiter_id = ?";
    private final String UPDATE_STATUS = "UPDATE waiter SET status = ? WHERE waiter_id = ?";
    private final String DELETE_WAITER = "UPDATE FROM waiter WHERE waiter_id = ?";
    private final String SELECT_ALL = "SELECT * FROM waiter";


    public WaiterDao(WrapperConnector wc) {
        connector = wc;
    }

    @Override
    public ArrayList<Waiter> getAll() {
        ArrayList<Waiter> waiters = new ArrayList<>();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_ALL)) {
            int i = 0;
            ResultSet resultSet = pstmt.executeQuery();
            while (resultSet.next()) {
                if(resultSet.getInt("status") == 0) {
                    Waiter waiter = new Waiter();
                    waiter.setId(resultSet.getInt("waiter_id"));
                    waiter.setName(resultSet.getString("full_name"));
                    waiter.setLogin(resultSet.getString("register_login"));
                    waiters.add(i++, waiter);
                }
            }
        } catch (SQLException e) {
            LOG.error(e);
        }
        return waiters;
    }

    @Override
    public Waiter findById(Integer waiterId) {
        Waiter waiter = new Waiter();
        try (PreparedStatement pstmt = connector.getStatement(SELECT_BY_ID)) {
            pstmt.setInt(1, waiterId);
            ResultSet resultSet = pstmt.executeQuery();
            if (resultSet.next()) {
                waiter.setId(waiterId);
                waiter.setName(resultSet.getString("full_name"));
                waiter.setLogin(resultSet.getString("register_login"));
            }
        } catch (SQLException e) {
            LOG.error(e);
        }
        return waiter;
    }

    @Override
    public boolean delete(Integer waiterId) {
        try (PreparedStatement pstmt = connector.getStatement(DELETE_WAITER)) {
            pstmt.setLong(1, waiterId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e);
        }
        return true;
    }

    @Override
    public Integer create(Waiter waiter) {
        try (PreparedStatement pstmt = connector.getStatement(INSERT_WAITER)) {
            pstmt.setString(1, waiter.getName());
            pstmt.setString(2, waiter.getLogin());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    public Waiter update(Waiter waiter) {
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_WAITER)) {
            pstmt.setString(1, waiter.getName());
            pstmt.setString(2, waiter.getLogin());
            pstmt.setInt(3, waiter.getId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return waiter;
    }

    public boolean updateStatus(Integer waiterId){
        try (PreparedStatement pstmt = connector.getStatement(UPDATE_STATUS)) {
            pstmt.setInt(1, 1);
            pstmt.setInt(2, waiterId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        return true;
    }
}
