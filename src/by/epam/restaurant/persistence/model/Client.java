package by.epam.restaurant.persistence.model;

/**
 * Object for work with clients
 */
public class Client extends Entity {
    private String name;
    private String login;
    private int order;

    public Client() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
