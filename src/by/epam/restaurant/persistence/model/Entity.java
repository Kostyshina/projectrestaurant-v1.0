package by.epam.restaurant.persistence.model;

/**
 * Class-parent for all of the entity objects
 */
//implements Serializable, Cloneable
public class Entity {
    private int id;
    public Entity() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
}
