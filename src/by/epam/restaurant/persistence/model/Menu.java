package by.epam.restaurant.persistence.model;

/**
 * Object for work with menu
 */
public class Menu extends Entity {
    private String dish;
    private String description;
    private double price;

    public Menu() {
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
