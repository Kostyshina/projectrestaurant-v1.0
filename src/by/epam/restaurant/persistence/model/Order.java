package by.epam.restaurant.persistence.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Object for work with orders
 */
public class Order extends Entity{
    private int client;
    private int condition;
    private int waiter;
    private Date date;
    private String conditionState;
    private String waiterName;
    private HashMap<Integer, OrderPoint> points;

    public Order() {
        points = new HashMap<>();
    }

    public int getClient() {
        return client;
    }

    public void setClient(int client) {
        this.client = client;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getWaiter() {
        return waiter;
    }

    public void setWaiter(int waiter) {
        this.waiter = waiter;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConditionState() {
        return conditionState;
    }

    public void setConditionState(String conditionState) {
        this.conditionState = conditionState;
    }

    public String getWaiterName() {
        return waiterName;
    }

    public void setWaiterName(String waiterName) {
        this.waiterName = waiterName;
    }

    public HashMap<Integer, OrderPoint> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<OrderPoint> list){
        int i = 0;
        for(OrderPoint point : list){
            points.put(i, point);
            i++;
        }
    }
}
