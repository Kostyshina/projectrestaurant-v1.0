package by.epam.restaurant.persistence.model;

/**
 * Object for work with order points
 */
public class OrderPoint extends Entity {
    private int orderId;
    private int productId;
    private int quantity;
    private Menu dish;

    public OrderPoint(){}

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Menu getDish() {
        return dish;
    }

    public void setDish(Menu dish) {
        this.dish = dish;
    }
}
