package by.epam.restaurant.persistence.model;

/**
 * Object for work with waiters
 */
public class Waiter extends Entity {
    private String name;
    private String login;

    public Waiter() {
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
