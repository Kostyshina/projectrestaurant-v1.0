package by.epam.restaurant.propertiesconf;

import java.util.ResourceBundle;

/**
 * Get page path
 */
public class ConfigManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private ConfigManager() { }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
