package by.epam.restaurant.propertiesconf;

import java.util.ResourceBundle;

/**
 * Print messages from configuration file
 */
public class MessageManager {
    private ResourceBundle resourceBundle;

    public MessageManager() {
        resourceBundle = ResourceBundle.getBundle("resources.message");
    }

    public String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
