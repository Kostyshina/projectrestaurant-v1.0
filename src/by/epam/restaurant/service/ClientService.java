package by.epam.restaurant.service;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.dao.ClientDao;
import by.epam.restaurant.persistence.model.Client;

import java.util.ArrayList;

/**
 * Class communication with ClientDao
 * @see by.epam.restaurant.persistence.dao.ClientDao
 */
public class ClientService {
    private ClientDao clientDAO;

    public ClientService(WrapperConnector wc) {
        clientDAO = new ClientDao(wc);
    }

    public ArrayList<Client> getAll(){
        return clientDAO.getAll();
    }

    public Client findById(int clientId) {
        Client client;
        client = clientDAO.findById(clientId);
        return client;
    }

    //Not used
    public void delete(int clientId) {
        clientDAO.delete(clientId);
    }

    public void createNew(String clientName, String clientLogin, int clientOrder) {
        Client client = new Client();
        client.setName(clientName);
        client.setLogin(clientLogin);
        client.setOrder(clientOrder);
        clientDAO.create(client);
    }

    public void update(int clientId, String clientName, String clientLogin, int clientOrder) {
        Client client = new Client();
        client.setId(clientId);
        client.setName(clientName);
        client.setLogin(clientLogin);
        client.setOrder(clientOrder);
        clientDAO.update(client);
    }
}
