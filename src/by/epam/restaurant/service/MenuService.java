package by.epam.restaurant.service;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.dao.MenuDao;
import by.epam.restaurant.persistence.model.Menu;

import java.util.ArrayList;

/**
 * Class communication with MenuDao
 * @see by.epam.restaurant.persistence.dao.MenuDao
 */
public class MenuService {
    private MenuDao menuDAO;

    public MenuService(WrapperConnector wc) {
        menuDAO = new MenuDao(wc);
    }

    public ArrayList<Menu> getAll(){
        return menuDAO.getAll();
    }

    public Menu findById(int clientId) {
        Menu dish;
        dish = menuDAO.findById(clientId);
        return dish;
    }

    public void delete(int dishId) {
        menuDAO.delete(dishId);
    }

    public void deleteDish(int dishId){
        menuDAO.updateStatus(dishId);
    }

    public void createNew(String dish, String description, double price) {
        Menu menuDish = new Menu();
        menuDish.setDish(dish);
        menuDish.setDescription(description);
        menuDish.setPrice(price);
        menuDAO.create(menuDish);
    }

    public void update(int dishId, String dish, String description, double price) {
        Menu menuDish = new Menu();
        menuDish.setId(dishId);
        menuDish.setDish(dish);
        menuDish.setDescription(description);
        menuDish.setPrice(price);
        menuDAO.update(menuDish);
    }

    public void updateDish(int dishId, String dish, String description, double price){
        createNew(dish, description, price);
        menuDAO.updateStatus(dishId);
    }
}
