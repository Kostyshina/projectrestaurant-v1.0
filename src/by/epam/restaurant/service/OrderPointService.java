package by.epam.restaurant.service;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.dao.OrderPointDao;
import by.epam.restaurant.persistence.model.OrderPoint;

import java.util.ArrayList;

/**
 * Class communication with OrderPointDao
 * @see by.epam.restaurant.persistence.dao.OrderPointDao
 */
public class OrderPointService {
    private OrderPointDao orderDAO;

    public OrderPointService(WrapperConnector wc) { orderDAO = new OrderPointDao(wc); }

    //Not used
    public ArrayList<OrderPoint> getAll(int orderId){ return  orderDAO.getAll(orderId); }

    public ArrayList<OrderPoint> getAll(){
        return orderDAO.getAll();
    }

    //Not used
    public OrderPoint findById(int pointId) {
        OrderPoint point;
        point = orderDAO.findById(pointId);
        return point;
    }

    public void deleteAll(int orderId){
        orderDAO.deleteAll(orderId);
    }

    public void delete(int pointId) {
        orderDAO.delete(pointId);
    }

    public void createNew(int order, int product, int quantity) {
        OrderPoint point = new OrderPoint();
        point.setOrderId(order);
        point.setProductId(product);
        point.setQuantity(quantity);
        orderDAO.create(point);
    }

    //Not used
    public void update(int id, int order, int product, int quantity) {
        OrderPoint point = new OrderPoint();
        point.setId(id);
        point.setOrderId(order);
        point.setProductId(product);
        point.setQuantity(quantity);
        orderDAO.update(point);
    }
}
