package by.epam.restaurant.service;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.dao.OrderDao;
import by.epam.restaurant.persistence.model.Order;

import java.util.ArrayList;
import java.util.Date;

/**
 * Class communication with OrderDao
 * @see by.epam.restaurant.persistence.dao.OrderDao
 */
public class OrderService {
    private OrderDao orderDAO;

    public OrderService(WrapperConnector wc) { orderDAO = new OrderDao(wc);
    }

    public Order getOrder(int orderId) {
        return orderDAO.getOrder(orderId);
    }

    public ArrayList<Order> getAll(int clientId){
        return orderDAO.getAll(clientId);
    }

    public ArrayList<Order> getAllWaiter(int waiterId){
        return orderDAO.getAllWaiter(waiterId);
    }

    public ArrayList<Order> getAllNew(){
        return orderDAO.getAllNew();
    }

    public ArrayList<Order> getAll(){
        return orderDAO.getAll();
    }

    public Order findById(int orderId) {
        Order order;
        order = orderDAO.findById(orderId);
        return order;
    }

    public void delete(int orderId) {
        orderDAO.delete(orderId);
    }

    public int createNew(int client, int waiter, Date date) {
        Order order = new Order();
        order.setClient(client);
        order.setCondition(0);
        order.setWaiter(waiter);
        order.setDate(date);
        return orderDAO.create(order);
    }

    public void update(int orderId, int client, int condition, int waiter, Date date) {
        Order order = new Order();
        order.setId(orderId);
        order.setClient(client);
        order.setCondition(condition);
        order.setWaiter(waiter);
        order.setDate(date);
        orderDAO.update(order);
    }
}
