package by.epam.restaurant.service;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.dao.UserDao;
import by.epam.restaurant.persistence.model.User;

import java.util.ArrayList;

/**
 * Class communication with UserDao
 * @see by.epam.restaurant.persistence.dao.UserDao
 */
public class RegisterService {
    private UserDao userDAO;

    public RegisterService(WrapperConnector wc) {
        userDAO = new UserDao(wc);
    }

    public ArrayList<User> getAll(){
        return userDAO.getAll();
    }

    //Not used
    public User findById(String login) {
        User user;
        user = userDAO.findById(login);
        return user;
    }

    public void deleteWaiter(String login){ userDAO.updateStatus(login);}

    public void delete(String login) {
        userDAO.delete(login);
    }

    public void createNew(String login, String password, String status) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setStatus(status);
        userDAO.create(user);
    }

    //Not used
    public void update(String login, String password, String status) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setStatus(status);
        userDAO.update(user);
    }
}
