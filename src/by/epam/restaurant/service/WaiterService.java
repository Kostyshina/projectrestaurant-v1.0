package by.epam.restaurant.service;

import by.epam.restaurant.persistence.connector.WrapperConnector;
import by.epam.restaurant.persistence.dao.WaiterDao;
import by.epam.restaurant.persistence.model.Waiter;

import java.util.ArrayList;

/**
 * Class communication with WaiterDao
 * @see by.epam.restaurant.persistence.dao.WaiterDao
 */
public class WaiterService {
    private WaiterDao waiterDao;

    public WaiterService(WrapperConnector wc) { waiterDao = new WaiterDao(wc);
    }

    public ArrayList<Waiter> getAll(){
        return waiterDao.getAll();
    }

    public Waiter findById(int waiterId) {
        Waiter waiter;
        waiter = waiterDao.findById(waiterId);
        return waiter;
    }

    public void delete(int waiterId) {
        waiterDao.delete(waiterId);
    }

    public void deleteWaiter(int waiterId) {
        waiterDao.updateStatus(waiterId);
    }

    public void createNew(String waiterName, String waiterLogin) {
        Waiter waiter = new Waiter();
        waiter.setName(waiterName);
        waiter.setLogin(waiterLogin);
        waiterDao.create(waiter);
    }

    public void update(int waiterId, String waiterName, String waiterLogin) {
        Waiter waiter = new Waiter();
        waiter.setId(waiterId);
        waiter.setName(waiterName);
        waiter.setLogin(waiterLogin);
        waiterDao.update(waiter);
    }
}
