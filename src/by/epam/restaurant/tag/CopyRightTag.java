package by.epam.restaurant.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Writing int footer
 */
public class CopyRightTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {
        String right = "2015. Restaurant. Copyright";
        try {
            JspWriter out = pageContext.getOut();
            out.write(right);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }


    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
