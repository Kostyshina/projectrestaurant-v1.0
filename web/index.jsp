<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 19.04.2015
  Time: 8:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:import url="jsp/common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="style/main.css">
    <title><fmt:message key="label.index.title"/></title>
</head>
<body>
<h1><fmt:message key="label.index.name"/></h1>
<div class="all">
    <form name="LoginForm" method="post" action="controller">
        <div class="left_box">
            <c:choose>
                <c:when test="${status eq 'admin'}">
                    <p><fmt:message key="locale.index.message"/></p>
                </c:when>
                <c:otherwise>
                    <div><input type="hidden" name="command" value="Login"></div>
                    <p><fmt:message key="locale.index.signin"/></p>
                    <div><input type="text" name="log" size="15" required
                                title="<fmt:message key="locale.index.register.required"/>"
                                placeholder="<fmt:message key="locale.index.register.login.placeholder"/>">*</div>
                    <div><input type="password" name="pass" size="15" required
                                title="<fmt:message key="locale.index.register.required"/>"
                                placeholder="<fmt:message key="locale.index.register.password.placeholder"/>">*</div>
                    <div><input type="submit" id="button" value="Sign in"></div>
                </c:otherwise>
            </c:choose>
        </div>
    </form>
    <form name="RegForm" method="post" action="controller">
        <div class="right_box">
            <div><input type="hidden" name="command" value="Register"></div>
            <p><fmt:message key="locale.index.register"/></p>

            <div><input type="text" name="login" size="15" required pattern="\w+"
                        title="<fmt:message key="locale.index.register.required"/>">
                <fmt:message key="locale.index.register.login"/>*</div>
            <div><input title="<fmt:message key="locale.index.register.password.title"/>"
                        type="password" name="password" size="15" required
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
                <fmt:message key="locale.index.register.password"/>*</div>
            <div><input title="<fmt:message key="locale.index.register.password.title"/>"
                        type="password" name="rpassword" size="15" required
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
                <fmt:message key="locale.index.register.rpassword"/>*</div>
            <div><input type="submit" value="Sign up"></div>
            <div style="font-size: 18px; color: red">${error}</div>
        </div>
    </form>
</div>
<footer><c:import url="jsp/common/footer.jsp" charEncoding="utf-8"/></footer>
</body>
</html>
