<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 17.05.2015
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:import url="../common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title></title>
</head>
<body>
<table class="main" cellpadding="0" cellspacing="10">
    <tr>
        <td valign="top">
            <table border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td><img src="../../images/where%20your%20towel.png" width="100px"></td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <form action="controller" method="post">
                <c:set value="${dish}" var="item"/>
                <table width="100%" border="0" cellpadding="0" cellspacing="5">
                    <tr><input type="hidden" name="id" value="${item.id}"></tr>
                    <tr>
                        <td><input type="text" name="dish" value="${item.dish}"></td>
                        <td><input type="text" name="price" value="${item.price}"></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="description" value="${item.getDescription()}" size="20"></td>
                    </tr>
                    <tr>
                        <c:choose>
                            <c:when test="${item.id != 0}">
                                <input type="hidden" name="command" value="Edit_Dish">
                                <td align="right">
                                    <input type="submit" value="Edit">
                                </td>
                            </c:when>
                            <c:otherwise>
                                <input type="hidden" name="command" value="Add_Dish">
                                <td align="right">
                                    <input type="submit" value="Create">
                                </td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
</body>
</html>
