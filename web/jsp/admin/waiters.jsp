<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 16.05.2015
  Time: 4:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:import url="../common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../../style/main.css">
    <title><fmt:message key="label.waiters.title"/></title>
    <style type="text/css">
        table{
            table-layout: fixed;
            width: 80%;
            margin: auto;
            top: 35px;
            border: 2px solid black;
            border-collapse: collapse;
            border-bottom: none;
        }
        td, th{
            padding: 5px;
        }
        th {
            background: grey;
            font-family: Averia Sans Libre;
            text-align: left;
        }
        td {
            border-bottom: 1px solid black
        }
    </style>
</head>
<body>
<h1><fmt:message key="label.waiters.name"/></h1>
<c:if test="${status eq 'admin'}">
    <a href="../../index.jsp"><input type="button" value="Create new"></a>
    <%--<form method="get" action="controller">
        <input type="hidden" name="command" value="Hire_Waiter">
        <input type="submit" value="Create new">
    </form>--%>
</c:if>
<c:if test="${not empty waiters}">
    <div>
        <table class="list">
            <thead>
            <tr>
                <th></th>
                <th>Full name</th>
                <th>Login</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${waiters}" var="item">
                <tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.login}</td>
                <td><a href="controller?command=Fire_Waiter&amp;itemId=${item.id}">x</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</c:if>
</body>
</html>
