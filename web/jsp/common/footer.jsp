<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 05.05.2015
  Time: 18:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <link rel="stylesheet" href="../../style/footer.css">
    <title></title>
</head>
<body>
<section class="footer-blocks">
    <div class="footer-block">
        <p>Browse</p>
        <nav>
            <ul>
                <li><a href="../../jsp/main.jsp">Main</a></li>
                <li><a href="../controller?command=Profile">Profile</a></li>
            </ul>
        </nav>
    </div>
    <div class="footer-block">
        <p>Connect with us</p>
        <nav>
            <ul>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Instagram</a></li>
                <li><a href="#">Google+</a></li>
            </ul>
        </nav>
    </div>
</section>
<ctg:copyright/>
</body>
</html>
