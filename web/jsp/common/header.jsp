<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 05.05.2015
  Time: 17:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="lang" value="${not empty param.lang ? param.lang :
  not empty lang ? lang : pageContext.request.locale}" scope="session"/>
<html>
<head>
    <link rel="stylesheet" href="../../style/header.css">
    <title></title>
</head>
<body>
<div id="fixed">
<div id="menu">
    <ul>
        <a href="../../jsp/main.jsp"><img src="../../images/logo.png" width="35"></a>
        <li><a href="../controller?command=Get_Menu">Menu</a></li>
        <li><a href="../controller?command=Profile">Profile</a></li>
        <li><a href="../controller?command=Logout">Sign out</a></li>
        <li>
            Language:
            <a href="controller?command=Locale&amp;lang=en_US"><img src="../../images/gb.gif" width="16"></a>
            <a href="controller?command=Locale&amp;lang=ru_RU"><img src="../../images/ru.gif"></a>
        </li>
    </ul>
</div>
</div>
</body>
</html>
