<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 10.05.2015
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:import url="common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title><fmt:message key="label.dish.title"/></title>
    </head>
<body>
    <table class="main" cellpadding="0" cellspacing="10">
        <tr>
            <td valign="top">
            <table border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td><img src="../images/where%20your%20towel.png" width="100px"></td>
                </tr>
            </table>
            </td>
            <td valign="top">
            <form action="controller" method="post">
                <c:set value="${dish}" var="item"/>
                <table width="100%" border="0" cellpadding="0" cellspacing="5">
                    <tr><input type="hidden" name="id" value="${item.id}"></tr>
                    <tr>
                        <td>${item.dish}</td>
                        <td>${item.price}</td>
                    </tr>
                    <tr>
                        <td><p>${item.getDescription()}<p></td>
                        <td><p><select size="1" name="quantity">
                            <option selected value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select></p></td>
                    </tr>
                    <tr>
                        <c:if test="${status eq 'client'}">
                            <input type="hidden" name="command" value="Order_Dish">
                            <td align="right">
                                <input type="submit" value="Order">
                            </td>
                        </c:if>
                    </tr>
                </table>
            </form>
            </td>
        </tr>
    </table>
</body>
</html>
