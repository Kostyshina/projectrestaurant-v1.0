<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 05.05.2015
  Time: 15:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<c:import url="common/header.jsp"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title><fmt:message key="label.main.title"/></title>
</head>
<body>
<div style="margin:0 auto; width: 900px">
    <h1><fmt:message key="label.main.name"/></h1>
    <img src="../images/the-hitchhikers-guide-to-the-galaxy_1.jpg" width="900">
    <p><fmt:message key="locale.main.about"/></p>
    <p><fmt:message key="locale.main.description"/></p>
</div>
<footer><c:import url="common/footer.jsp" charEncoding="utf-8"/></footer>
</body>
</html>
