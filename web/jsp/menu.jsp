<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 20.04.2015
  Time: 13:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:import url="common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title><fmt:message key="label.menu.title"/></title>
    <script src="../js/jquery-1.2.6.js"></script>
    <style type="text/css">
        table{
            table-layout: fixed;
            width: 80%;
            margin: auto;
            top: 35px;
            border: 2px solid black;
            border-collapse: collapse;
            border-bottom: none;
        }
        td, th{
            padding: 5px;
        }
        th {
            background: grey;
            font-family: Averia Sans Libre;
            text-align: left;
        }
        td {
            border-bottom: 1px solid black
        }
    </style>
    <script type="text/javascript">
        window.onload= function() {
            var div = document.getElementById('first');
            div.onclick = function() {
                openbox('all_menu', div);
                return false;
            };
        };
        function openbox(id, image) {
            var div = document.getElementById(id);
            if(div.style.display == 'block') {
                div.style.display = 'none';
            }
            else {
                div.style.display = 'block';
                image.style.display = 'none';
            }
        }


        jQuery( function($) {
            $('tbody tr[data-href]').click( function() {
                location.href = $(this).attr('data-href');
            });
        });
    </script>
</head>
<body>
<div id="first" style="display: block; margin: 100px auto; width: 400px">
    <img src="../images/don't panic.jpg" width="400">
</div>
<div id="all_menu" style="display: none; margin: 100px auto; width: 1000px">
    <div style="width: 40px; margin: 0 auto; padding-bottom: 20px"><img src="../images/arrow left.png" width="40"></div>
    <div style="margin: 0 auto;">
        <%--<img src="../images/menu.jpg">--%>
        <c:if test="${not empty menu}">
            <div>
                <table class="list">
                    <thead>
                    <tr>
                        <th>Dish</th>
                        <th>Description</th>
                        <th>Price</th>
                        <c:if test="${status eq 'admin'}">
                            <th></th>
                        </c:if>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${menu}" var="item">
                        <tr data-href="controller?command=Get_Dish&amp;itemId=${item.id}">
                            <td>${item.dish}</td>
                            <td>${item.description}</td>
                            <td>${item.price}</td>
                            <c:if test="${status eq 'admin'}">
                                <td><a href="controller?command=Delete_Dish&amp;itemId=${item.id}">x</a></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:if>
    </div>
    <div style="width: 40px; margin: 0 auto; padding-bottom: 20px"><img src="../images/arrow right.png" width="40"></div>
    <c:if test="${status eq 'admin'}">
        <a href="controller?command=Get_Dish&amp;itemId=0"><input type="button" value="Create new dish"></a>
    </c:if>
</div>
</body>
<footer><c:import url="common/footer.jsp" charEncoding="utf-8"/></footer>
</html>
