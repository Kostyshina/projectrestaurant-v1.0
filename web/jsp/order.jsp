<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.05.2015
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:import url="common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title>Order</title>
</head>
<body>
<h1><fmt:message key="label.order.name"/></h1>
<table class="main" cellpadding="0" cellspacing="10">
    <tr>
        <td valign="top">
            <table border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td><img src="../images/where%20your%20towel.png" width="200px"></td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <form action="controller" method="post">
                <c:choose>
                    <c:when test="${status eq 'client'}">
                        <input type="hidden" name="command" value="Make_an_Order">
                    </c:when>
                    <c:otherwise>
                        <input type="hidden" name="command" value="Take_an_Order">
                    </c:otherwise>
                </c:choose>
                <c:set value="${order}" var="item"/>
                <c:if test="${(status eq 'waiter') && (item.condition == 2)}">
                    <a href="controller?command=Bill&amp;itemId=${item.id}">A/P</a>
                </c:if>
                <table width="100%" border="0" cellpadding="0" cellspacing="5">
                    <tr><input type="hidden" name="id" value="${item.id}"></tr>
                    <tr>
                        <td>${item.waiterName}</td>
                        <td>${item.conditionState}</td>
                    </tr>
                    <c:forEach items="${item.points}" var="point">
                        <c:set value="${point.value.dish}" var="dish"/>
                        <tr>
                            <td>${dish.dish}</td>
                            <td><p>${dish.description}<p></td>
                            <td>${dish.price}</td>
                            <td>${point.value.quantity}</td>
                            <c:if test="${(status eq 'client') && (item.condition == 0)}">
                                <td><a href="controller?command=Edit_Order&amp;itemId=${point.value.id}&amp;order=${item.id}">x</a></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td>${cost}</td>
                    </tr>
                    <tr>
                        <c:choose>
                            <c:when test="${status eq 'client'}">
                                <c:if test="${item.condition < 2}">
                                    <td>
                                        <a href="controller?command=Cancel_Order&amp;orderId=${item.id}">
                                            <input type="button" value="Cancel">
                                        </a>
                                    </td>
                                    <td align="right">
                                        <input type="submit" value="Order">
                                    </td>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <c:if test="${item.condition == 1}">
                                    <td align="right">
                                        <input type="submit" value="Take">
                                    </td>
                                </c:if>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
</body>
</html>
