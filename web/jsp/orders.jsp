<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 14.05.2015
  Time: 14:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:import url="common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title><fmt:message key="label.orders.title"/></title>
    <script src="../js/jquery-1.2.6.js"></script>
    <script type="text/javascript">
        jQuery( function($) {
            $('tbody tr[data-href]').click( function() {
                location.href = $(this).attr('data-href');
            });
        });
    </script>
</head>
<body>
<h1><fmt:message key="label.orders.name"/></h1>
<c:if test="${not empty orders}">
    <table class="main" cellpadding="0" cellspacing="10">
        <tr>
            <td valign="top">
                <c:forEach items="${orders}" var="order">
                    <table width="100%" border="0" cellpadding="0" cellspacing="5">
                        <tr><input type="hidden" name="id" value="${order.id}"></tr>
                        <c:choose>
                            <c:when test="${status eq 'waiter'}">
                                <tr data-href="controller?command=Get_Order&amp;order=${order.id}">
                             </c:when>
                            <c:otherwise>
                                <tr>
                            </c:otherwise>
                        </c:choose>
                            <c:if test="${(status eq 'admin') || (status eq 'waiter')}">
                                <td>${order.client}</td>
                            </c:if>
                            <c:if test="${(status eq 'admin') || (status eq 'client')}">
                                <td>${order.waiterName}</td>
                            </c:if>
                            <td>${order.conditionState}</td>
                        </tr>
                        <c:forEach items="${order.points}" var="point">
                            <c:set value="${point.value.dish}" var="dish"/>
                            <tr>
                                <td>${dish.dish}</td>
                                <td><p>${dish.description}<p></td>
                                <td>${dish.price}</td>
                                <td>${point.value.quantity}</td>
                                    <%--<td><a href="controller?command=Delete_Dish&amp;itemId=${point.value.id}">x</a></td>--%>
                            </tr>
                        </c:forEach>
                    </table>
                </c:forEach>
            </td>
        </tr>
    </table>
</c:if>
<c:if test="${not empty newOrders}">
    <table class="main" cellpadding="0" cellspacing="10">
        <tr>
            <td valign="top">
                <c:forEach items="${newOrders}" var="item">
                    <table width="100%" border="0" cellpadding="0" cellspacing="5">
                        <tr><input type="hidden" name="id" value="${item.id}"></tr>
                        <tr data-href="controller?command=Get_Client&amp;itemId=${item.id}">
                            <td>${item.client}</td>
                            <c:if test="${(status eq 'admin') || (status eq 'client')}">
                                <td>${item.waiterName}</td>
                            </c:if>
                            <td>${item.conditionState}</td>
                        </tr>
                        <c:forEach items="${item.points}" var="point">
                            <c:set value="${point.value.dish}" var="dish"/>
                            <tr>
                                <td>${dish.dish}</td>
                                <td><p>${dish.description}<p></td>
                                <td>${dish.price}</td>
                                <td>${point.value.quantity}</td>
                                    <%--<td><a href="controller?command=Delete_Dish&amp;itemId=${point.value.id}">x</a></td>--%>
                            </tr>
                        </c:forEach>
                    </table>
                </c:forEach>
            </td>
        </tr>
    </table>
</c:if>
</body>
</html>
