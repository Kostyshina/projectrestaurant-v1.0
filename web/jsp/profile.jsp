<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 20.04.2015
  Time: 13:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:import url="common/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="resources.page"/>
<html>
<head>
    <link rel="stylesheet" href="../style/main.css">
    <title><fmt:message key="label.profile.title"/></title>
    <script type="text/javascript" src="../js/jquery-1.2.6.js"></script>
    <style type="text/css">
        table{
            table-layout: fixed;
            width: 80%;
            margin: auto;
            top: 35px;
            border: 2px solid black;
            border-collapse: collapse;
            border-bottom: none;
        }
        td, th{
            padding: 5px;
        }
        th {
            background: grey;
            font-family: Averia Sans Libre;
            text-align: left;
        }
        td {
            border-bottom: 1px solid black
        }
    </style>
    <script type="text/javascript">
        jQuery( function($) {
            $('tbody tr[data-href]').click( function() {
                location.href = $(this).attr('data-href');
            });
        });
    </script>
</head>
<body>
<div style="margin: 0 auto; width: 900px">
    <h1><fmt:message key="label.profile.name"/></h1>
</div>
<table class="main" cellpadding="0" cellspacing="10">
    <tr>
        <td valign="top">
            <a href="controller?command=All_Orders"><fmt:message key="locale.profile.orders"/></a>
            <c:if test="${status eq 'admin'}">
                <a href="controller?command=All_Waiters"><fmt:message key="locale.profile.waiters"/></a>
            </c:if>
            <form action="controller" method="post">
                <input type="hidden" name="command" value="Edit_Profile">
                <c:set value="${user}" var="item"/>
                <table width="100%" border="0" cellpadding="0" cellspacing="4">
                    <tr><input type="hidden" name="id" value="${item.id}"></tr>
                    <tr>
                        <td><input type="text" name="userLogin" value="${item.login}" size="15" readonly></td>
                        <td><fmt:message key="locale.profile.login"/></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="userName" value="${item.name}" size="15"></td>
                        <td><fmt:message key="locale.profile.name"/></td>
                    </tr>
                    <%--//отдельно смену пароля--%>
                    <%--<tr>
                        <td><input type="password" name="userPassword" value="${userPassword}" size="15"
                                required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"></td>
                        <td><fmt:message key="locale.profile.password"/></td>
                    </tr>--%>
                    <%--<tr>
                        <td><input type="password" name="userRpassword"
                                size="15" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"></td>
                        <td><fmt:message key="locale.profile.rpassword"/></td>
                    </tr>--%>
                    <tr>
                        <td><input type="text" name="userStatus" value="${status}" size="15" readonly></td>
                        <td><fmt:message key="locale.profile.status"/></td>
                    </tr>
                    <c:if test="${status eq 'client'}">
                    <tr data-href="controller?command=Get_Order&amp;order=${item.order}">
                        <td><input type="text" name="userOrder" value="${item.order}" size="15" readonly></td>
                        <td><fmt:message key="locale.profile.order"/></td>
                    </tr>
                    </c:if>
                </table>
                <div><input type="submit" value="Save"></div>
            </form>
        </td>
    </tr>
</table>
<footer><c:import url="common/footer.jsp" charEncoding="utf-8"/></footer>
</body>
</html>